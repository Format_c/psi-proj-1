if (typeof String.prototype.startsWith != 'function') {
    String.prototype.startsWith = function (str){
        return this.slice(0, str.length) == str;
    };
}

function lightbox() {
    $('body').append('<div class="overlay" id="overlay"><div class="image"><img src="" alt="" data-index=""><div class="close"><i class="fa fa-times"></i></div><div class="prev"><i class="fa fa-chevron-left"></i></div><div class="next"><i class="fa fa-chevron-right"></i></div><div class="rating"></div></div></div>');
}

function socialList(social) {
    return '<span><a href="'+social.link+'"><i class="fa fa-'+social.icon+'"></i> '+social.name+'</a></span>';
}

function addRider(rider) {
    var result = '<div class="rider"><h1 id="riders-'+rider.last_name+'">'+rider.name+'</h1><div class="desc"><p><i class="fa fa-calendar"></i> <strong>Born:</strong> '+rider.born+'</p><p><i class="fa fa-map-marker"></i> <strong>From:</strong> '+rider.city+'</p><p><i class="fa fa-bicycle"></i> <strong>Bicycle:</strong> '+rider.bicycle+'</p><p><i class="fa fa-trophy"></i> <strong>Place: </strong> '+rider.place+'</p><div class="social">';
    result += rider.social.map(socialList).join('\n');
    result += '</div></div><div class="big-photo"><a class="gallery" rel="'+rider.last_name+'" href="images/'+rider.last_name+'/'+rider.photo+'"><img src="" data-src="images/'+rider.last_name+'/m_'+rider.photo+'" alt="'+rider.name+'"></a></div><br class="clear"><div class="photos">';
    result += rider.photos.map(function(photo) {
        return '<a class="gallery" rel="'+rider.last_name+'" href="images/'+rider.last_name+'/'+photo+'"><img src="" data-src="images/'+rider.last_name+'/m_'+photo+'" alt=""></a>';
    }).join('');
    result += '</div></div>';
    return result;
}

jQuery(document).ready(function($) {
    lightbox();
    var photoSet = [],
        lastScrollTop = 0;

    function checkGallery(index) {
        if (index === 0) {
            $('.prev').hide(0);
        }
        else {
            $('.prev').show(0);
        }
        if (index === (photoSet.length - 1)) {
            $('.next').hide(0);
        }
        else {
            $('.next').show(0);
        }
    }

    function renderRating(key) {
        $('.rating').html('<i class="fa fa-star-o" data-rate="1"></i><i class="fa fa-star-o" data-rate="2"></i><i class="fa fa-star-o" data-rate="3"></i><i class="fa fa-star-o" data-rate="4"></i><i class="fa fa-star-o" data-rate="5"></i>');
    }

    function checkRate(key, rate) {
        rate = rate || localStorage.getItem(key) || 0;
        $('#overlay .fa').each(function(index, el) {
            if ($(el).attr('data-rate') <= rate) {
                $(el).removeClass('fa-star-o').addClass('fa-star');
            }
            else {
                $(el).removeClass('fa-star').addClass('fa-star-o');
            }
        });
    }

    function checkVisibility() {
        var visibleArea = $(window).scrollTop() + window.innerHeight;
        $('.rider img').filter(function() {
            return $(this).offset().top < visibleArea;
        }).filter(function() {
            return $(this).attr('src') === '';
        }).map(function() {
            $(this).attr('src', $(this).attr('data-src'));
        });
    }

    function spyScroll () {
        var riders = $('.rider'),
            browserOffset = window.innerHeight / 3,
            scrolledRiders = riders.filter(function() {
                return ($(this).offset().top - $(window).scrollTop() - browserOffset) < 0;
            }),
            activeRider = scrolledRiders.last().children('h1').attr('id');
        $('.right-menu a').removeClass('active');
        $('.right-menu a[href="#'+activeRider+'"]').addClass('active');
        history.replaceState({}, '', '#'+activeRider);
    }

    function getPhotosFromLocalStorage (sorting) {
        $('.photos').html('');
        var photos = [];
        for ( var i = 0, len = localStorage.length; i < len; ++i ) {
            photos.push({
                rider: localStorage.key(i).split('/')[1],
                file: localStorage.key(i).split('/')[2],
                rate: localStorage.getItem(localStorage.key(i))
            });
        }
        photos.sort(sorting);
        for (var j = photos.length - 1; j >= 0; j--) {
            $('.photos').append('<a class="gallery" rel="rated" href="images/'+photos[j].rider+'/'+photos[j].file+'"><img src="images/'+photos[j].rider+'/m_'+photos[j].file+'" alt=""></a>');
        }
    }

    function loadPage(page) {
        if (page === '#home') {
            if (!($('.logo').hasClass('hidden'))) {
                $('.logo').fadeOut(500, function() {
                    $('.logo').addClass('hidden');
                });
            }
        }
        else {
            if ($('.logo').hasClass('hidden')) {
                $('.logo').fadeIn(500, function() {
                    $('.logo').removeClass('hidden');
                });
            }
        }
        if (page != '#rated') {
            $('body').addClass('loading');
            $.ajax({
                url: 'index.php',
                type: 'GET',
                dataType: 'json',
                data: {page: page.slice(1)},
            })
            .done(function(response) {
                if (response.type === 'html') {
                    $('.content-wrapper').html(response.content);
                    if (!($('.right-menu').hasClass('hidden'))) {
                        $('.right-menu').fadeOut(500, function() {
                            $('.right-menu').addClass('hidden');
                        });
                    }
                }
                else if (response.type === 'riders_array') {
                    $('.content-wrapper').html('');
                    for (var i = 0; i < response.content.length; i++) {
                        $('.content-wrapper').append(addRider(response.content[i]));
                    }
                    checkVisibility();
                    spyScroll();
                    if ($('.right-menu').hasClass('hidden')) {
                        $('.right-menu').fadeIn(500, function() {
                            $('.right-menu').removeClass('hidden');
                        });
                    }
                }
            })
            .always(function() {
                $('body').removeClass('loading');
            });
            
        }
        else {
            // Show rated photos
            if (localStorage.length > 0) {
                $('.content-wrapper').html('<div class="sorting"><i class="fa fa-sort"></i> <strong>Sort by:</strong><ul><li><a href="#" class="active">Rate</a></li><li><a href="#">Name</a></li><li><a href="#">Shuffle</a></li></ul></div><div class="photos"></div>');
                getPhotosFromLocalStorage(function(a, b) {
                    return parseInt(a.rate) - parseInt(b.rate);
                });
            }
            else {
                $('.content-wrapper').html('<p>There are no rated photos</p>');
            }
            if (!($('.right-menu').hasClass('hidden'))) {
                $('.right-menu').fadeOut(500, function() {
                    $('.right-menu').addClass('hidden');
                });
            }
        }
    } 

    $('.content-wrapper').on('click', '.gallery', function(event) {
        event.preventDefault();
        var that = $(this),
            source = that.attr('href'),
            rel = that.attr('rel');
        if (rel) {
            photoSet = $('.gallery[rel="'+rel+'"]');
        }
        else {
            photoSet = [that];
        }

        checkGallery(photoSet.index(that));
        renderRating(source);
        checkRate(source);

        $('#overlay img').attr('src', '');
        $('#overlay img').attr({'src': source, 'data-index': photoSet.index(that)});
        $('#overlay').fadeIn(500);
    });

    $('.content-wrapper').on('click', '.sorting a', function(event) {
        event.preventDefault();
        $('.sorting a').removeClass('active');
        $(this).addClass('active');
        if ($(this).text() === 'Rate') {
            getPhotosFromLocalStorage(function(a, b) {
                return parseInt(a.rate) - parseInt(b.rate);
            });
        }
        else if($(this).text() === 'Name'){
            getPhotosFromLocalStorage(function(a, b) {
                return b.rider.localeCompare(a.rider);
            });
        }
        else {
            getPhotosFromLocalStorage(function(a, b) {
                return 0.5 - Math.random();
            }); 
        }
    });

    $('#overlay').on('click', '.close', function(event) {
        event.preventDefault();
        event.stopPropagation();
        $('#overlay').fadeOut(500);
    });

    $('#overlay').on('click', function(event) {
        event.preventDefault();
        $('#overlay').fadeOut(500);
    });

    $('#overlay').on('click', '.next', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var index = $('#overlay img').attr('data-index');
        index++;
        var source = $(photoSet[index]).attr('href');
        $('#overlay .image').addClass('old');
        $('#overlay').prepend('<div class="image new"><img src="'+source+'" alt="" data-index="'+index+'"><div class="close"><i class="fa fa-times"></i></div><div class="prev"><i class="fa fa-chevron-left"></i></div><div class="next"><i class="fa fa-chevron-right"></i></div><div class="rating"></div></div>');
        $('#overlay .image.old').fadeOut('500', function() {
            $('#overlay .image.old').remove();
            $('#overlay .image.new').removeClass('new');
            checkGallery(index);
            renderRating(source);
            checkRate(source);
        });
    });

    $('#overlay').on('click', '.prev', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var index = $('#overlay img').attr('data-index');
        index--;
        var source = $(photoSet[index]).attr('href');
        $('#overlay .image').addClass('old');
        $('#overlay').prepend('<div class="image new"><img src="'+source+'" alt="" data-index="'+index+'"><div class="close"><i class="fa fa-times"></i></div><div class="prev"><i class="fa fa-chevron-left"></i></div><div class="next"><i class="fa fa-chevron-right"></i></div><div class="rating"></div></div>');
        $('#overlay .image.old').fadeOut('500', function() {
            $('#overlay .image.old').remove();
            $('#overlay .image.new').removeClass('new');
            checkGallery(index);
            renderRating(source);
            checkRate(source);
        });
    });

    $('#overlay').on('mouseenter', '.rating .fa', function(event) {
        event.preventDefault();
        var rate = $(this).attr('data-rate'),
            source = $('#overlay img').attr('src');
        checkRate(source, rate);
    });

    $('#overlay').on('mouseleave', '.rating', function(event) {
        event.preventDefault();
        var source = $('#overlay img').attr('src');
        checkRate(source);
    });

    $('#overlay').on('click', '.rating .fa', function(event) {
        event.preventDefault();
        event.stopPropagation();
        var rate = $(this).attr('data-rate'),
            source = $('#overlay img').attr('src');
        if (localStorage.getItem(source) === rate) {
            localStorage.removeItem(source);
        }
        else {
            localStorage.setItem(source, rate);
        }
    });

    $(window).on('scroll', function(event) {
        var bgPos = $(window).scrollTop() / 1.2;
        $('.bg-main').css('background-position', '0 '+bgPos+'px');

        if (!($('.right-menu').hasClass('hidden'))) {
            event.preventDefault();
            checkVisibility();
            spyScroll();
        }
    });

    $('.right-menu').on('click', 'a', function(event) {
        event.preventDefault();
        var rider = $(this).attr('href'),
            riderOffset = $(rider).offset().top;
        $('html, body').animate({ scrollTop: riderOffset}, 500);
    });

    $('.top-menu').on('click', 'a', function(event) {
        event.preventDefault();
        var page = $(this).attr('href');
        history.pushState({}, '', page);
        loadPage(page);
    });

    var page = document.location.hash;
    if (page) {
        if (page.startsWith('#riders')) {
            page = '#riders';
        }
        loadPage(page);
    }
});
