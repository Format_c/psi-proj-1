<?php
if (!empty($_GET["page"])) {
    switch ($_GET["page"]) {
        case 'about':
            $response = '<h1>About Event</h1>
                <p>The <strong>Red Bull Rampage</strong> is an invite-only freeride mountain bike competition held near <strong>Zion National Park</strong> in <strong>Virgin, Utah, United States</strong>, just to the north of <strong>Gooseberry Mesa</strong>. Previously it was held off the Kolob Terrace Road, on the western boundary of <strong>Zion National Park</strong>.</p>
                <h1>Results</h1>
                <p>Full Replay of Rampage 2015 available on <a href="http://www.redbull.tv/videos/event-stream-556/mountain-bike-freeride-thrills-from-utah-usa?cmpid=ocp-youtuberampage2015">redbull.tv</a> website.</p>
                <table>
                    <tr>
                        <th>Place</th>
                        <th>Athlete</th>
                        <th>Score Run 1</th>
                        <th>Score Run 2</th>
                    </tr>
                    <tr>
                        <td>1</td>
                        <td>Kurt Sorge</td>
                        <td>96.50</td>
                        <td>Not taking 2nd run</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>Andreu Lacondeguy</td>
                        <td>95.75</td>
                        <td>Wind - Not taking 2nd run</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>Graham Agassuz</td>
                        <td>94.75</td>
                        <td>Wind - Not taking 2nd run</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>Brandon Semenuk</td>
                        <td>94</td>
                        <td>94.25</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>Thomas Genon</td>
                        <td>91.25</td>
                        <td>Not taking 2nd run</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>Cam Zink</td>
                        <td>89.25</td>
                        <td>Not taking 2nd run</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>Darren Berrecloth</td>
                        <td>DNF (Missed Line)</td>
                        <td>87</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>Brendan Fairclough</td>
                        <td>77</td>
                        <td>85.5</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>Sam Reynolds</td>
                        <td>83</td>
                        <td>40 (Crash)</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>Remy Metailler</td>
                        <td>80</td>
                        <td>82.5</td>
                    </tr>
                    <tr>
                        <td>11</td>
                        <td>Kyle Strait</td>
                        <td>82</td>
                        <td>Not taking 2nd run</td>
                    </tr>
                    <tr>
                        <td>12</td>
                        <td>Pierre Edouard Ferry</td>
                        <td>78.50</td>
                        <td>81.75</td>
                    </tr>
                    <tr>
                        <td>13</td>
                        <td>Brett Rheeder</td>
                        <td>81.50</td>
                        <td>51.75 (Crash)</td>
                    </tr>
                    <tr>
                        <td>14</td>
                        <td>Kyle Norbraten</td>
                        <td>80.50</td>
                        <td>Not taking 2nd run</td>
                    </tr>
                    <tr>
                        <td>15</td>
                        <td>Logan Binggeli</td>
                        <td>75.50</td>
                        <td>66.75</td>
                    </tr>
                    <tr>
                        <td>16</td>
                        <td>Bas Van Steenbergen</td>
                        <td>70.50</td>
                        <td>70.50</td>
                    </tr>
                    <tr>
                        <td>17</td>
                        <td>Ryan Howard</td>
                        <td>66.75</td>
                        <td>64.75</td>
                    </tr>
                    <tr>
                        <td>18</td>
                        <td>Mitch Chubey</td>
                        <td>63.25</td>
                        <td>DNF</td>
                    </tr>
                    <tr>
                        <td>19</td>
                        <td>Antoine Bizet</td>
                        <td>62.25</td>
                        <td>62</td>
                    </tr>
                    <tr>
                        <td>-</td>
                        <td>Kelly McGarry</td>
                        <td>DNF (Crash)</td>
                        <td>-</td>
                    </tr>
                    <tr>
                        <td>-</td>
                        <td>Paul Basagoitia</td>
                        <td>DNF (Crash)</td>
                        <td>-</td>
                    </tr>
                </table>
                <h1>Location</h1>
                <p><strong>Virgin, Utah, United States</strong></p>
                <iframe
                  width="600"
                  height="450"
                  frameborder="0" style="border:0"
                  src="https://www.google.com/maps/embed/v1/place?key=AIzaSyB9Yj4-kICUniCtLl0kVslf_rjU9dUtqPc&q=Virgin,+UT,+USA" allowfullscreen>
                </iframe>';
            echo json_encode(array('content' => $response, 'type' => 'html'));
            break;

        case 'riders':
            $response = array(
                array(
                    'name' => 'Kurt Sorge',
                    'last_name' => 'sorge',
                    'born' => 'November 28, 1988 (age 26)',
                    'city' => 'Nelson, British Columbia, Canada',
                    'bicycle' => 'Polygon DH9',
                    'place' => '1st (96.50)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/kurtsorgefanpage',
                            'name' => '/kurtsorgefanpage'
                            ),
                        array (
                            'icon' => 'twitter',
                            'link' => 'https://twitter.com/KurtSorge',
                            'name' => '@KurtSorge'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/kurtsorge',
                            'name' => 'kurtsorge'
                            )
                        ),
                    'photo' => 'p5pb12791706.jpg',
                    'photos' => array (
                        'p5pb12797037.jpg',
                        'p5pb12797076.jpg',
                        'p5pb12797187.jpg',
                        'p5pb12797208.jpg',
                        'p5pb12797215.jpg',
                        'p5pb12797217.jpg',
                        'p5pb12797236.jpg',
                        'p5pb12797326.jpg',
                        'p5pb12797358.jpg',
                        'p5pb12797377.jpg'
                        )
                    ),
                array(
                    'name' => 'Andreu Lacondeguy',
                    'last_name' => 'lacondeguy',
                    'born' => 'January 12, 1989 (age 26)',
                    'city' => 'Linás del Vallés, Barcelona, Spain',
                    'bicycle' => 'YT TUES 2.0',
                    'place' => '2nd (95.75)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/AndreuLacondeguyOfficialFanpage',
                            'name' => '/AndreuLacondeguyOfficialFanpage'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/andreulacondeguy',
                            'name' => 'andreulacondeguy'
                            ),
                        array (
                            'icon' => 'globe',
                            'link' => 'http://www.redbull.com/en/bike/athletes/1326300931306/andreu-lacondeguy',
                            'name' => 'RedBull.com'
                            )
                        ),
                    'photo' => 'p0pb11458101.jpg',
                    'photos' => array (
                        'p5pb12797168.jpg',
                        'p5pb12797219.jpg',
                        'p5pb12797222.jpg',
                        'p5pb12797400.jpg'
                        )
                    ),
                array(
                    'name' => 'Graham Agassiz',
                    'last_name' => 'agassiz',
                    'born' => 'January 8, 1990 (age 25)',
                    'city' => 'Kamloops, British Columbia, Canada',
                    'bicycle' => 'Kona Supreme Operator',
                    'place' => '3rd (94.75)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/Graham-Agassiz-217516354940977/',
                            'name' => 'Graham Agassiz'
                            ),
                        array (
                            'icon' => 'twitter',
                            'link' => 'https://twitter.com/grahamagassiz',
                            'name' => '@GrahamAgassiz'
                            ),
                        ),
                    'photo' => 'p5pb12791722.jpg',
                    'photos' => array (
                        'p5pb12797043.jpg',
                        'p5pb12797075.jpg',
                        'p5pb12797180.jpg',
                        'p5pb12797211.jpg',
                        'p5pb12797213.jpg',
                        'p5pb12797253.jpg',
                        'p5pb12797305.jpg',
                        'p5pb12797349.jpg',
                        'p5pb12797373.jpg'
                        )
                    ),
                array(
                    'name' => 'Brandon Semenuk',
                    'last_name' => 'semenuk',
                    'born' => '2 March 1991 (age 24)',
                    'city' => 'Whistler, British Columbia, Canada',
                    'bicycle' => 'Trek Session Park',
                    'place' => '4th (94.25)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/Brandon-Semenuk-172128549470965/',
                            'name' => 'Brandon Semenuk'
                            ),
                        array (
                            'icon' => 'twitter',
                            'link' => 'https://twitter.com/brandonsemenuk',
                            'name' => '@BrandonSemenuk'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/brandonsemenuk',
                            'name' => 'brandonsemenuk'
                            ),
                        array (
                            'icon' => 'globe',
                            'link' => 'http://www.redbull.com/en/bike/athletes/1326300811760/brandon-semenuk',
                            'name' => 'RedBull.com'
                            )
                        ),
                    'photo' => 'p5pb12791691.jpg',
                    'photos' => array (
                        'p5pb12797026.jpg',
                        'p5pb12797028.jpg',
                        'p5pb12797170.jpg',
                        'p5pb12797171.jpg',
                        'p5pb12797179.jpg',
                        'p5pb12797190.jpg',
                        'p5pb12797197.jpg',
                        'p5pb12797229.jpg',
                        'p5pb12797234.jpg',
                        'p5pb12797321.jpg',
                        'p5pb12797347.jpg',
                        'p5pb12797351.jpg',
                        'p5pb12797378.jpg',
                        'p5pb12797381.jpg'
                        )
                    ),
                array(
                    'name' => 'Thomas Genon',
                    'last_name' => 'genon',
                    'born' => '18 August 1993 (age 22)',
                    'city' => 'Belgium',
                    'bicycle' => 'Canyon Torque DHX',
                    'place' => '5th (91.25)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/Thomas-genon-245186952253314/',
                            'name' => 'Thomas Genon'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/thomasgenon/',
                            'name' => 'thomasgenon'
                            ),
                        array (
                            'icon' => 'globe',
                            'link' => 'http://www.redbull.com/en/bike/athletes/1331580318015/thomas-genon',
                            'name' => 'RedBull.com'
                            )
                        ),
                    'photo' => 'p5pb12791704.jpg',
                    'photos' => array (
                        'p5pb12797064.jpg',
                        'p5pb12797220.jpg',
                        'p5pb12797359.jpg'
                        )
                    ),
                array(
                    'name' => 'Cam Zink',
                    'last_name' => 'zink',
                    'born' => 'March 8, 1986 (age 29)',
                    'city' => 'Reno, Nevada, United States',
                    'bicycle' => 'YT Tues CF',
                    'place' => '6th (89.25)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/Cam-Zink-121214941259631/',
                            'name' => 'Cam Zink'
                            ),
                        array (
                            'icon' => 'twitter',
                            'link' => 'https://twitter.com/CamZink',
                            'name' => '@CamZink'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/camzink/',
                            'name' => 'camzink'
                            )
                        ),
                    'photo' => 'p5pb12791693.jpg',
                    'photos' => array (
                        'p5pb12797029.jpg',
                        'p5pb12797183.jpg',
                        'p5pb12797184.jpg',
                        'p5pb12797288.jpg'
                        )
                    ),
                array(
                    'name' => 'Darren Berrecloth',
                    'last_name' => 'berrecloth',
                    'born' => 'October 30, 1981 (age 33)',
                    'city' => 'Parksville, Canada',
                    'bicycle' => 'Canyon Torque DHX',
                    'place' => '7th (87.00)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com//darren.berrecloth.9',
                            'name' => '/darren.berrecloth.9'
                            ),
                        array (
                            'icon' => 'twitter',
                            'link' => 'https://twitter.com/darenberrecloth',
                            'name' => '@darenberrecloth'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/dberrecloth/',
                            'name' => 'dberrecloth'
                            ),
                        array (
                            'icon' => 'globe',
                            'link' => 'http://www.redbull.com/en/bike/athletes/1326300812365/darren-berrecloth',
                            'name' => 'RedBull.com'
                            ),
                        array (
                            'icon' => 'globe',
                            'link' => 'http://darrenberrecloth.com/',
                            'name' => 'Official Website'
                            )
                        ),
                    'photo' => 'p5pb12791727.jpg',
                    'photos' => array (
                        'p5pb12797027.jpg',
                        'p5pb12797068.jpg',
                        'p5pb12797266.jpg',
                        'p5pb12797323.jpg'
                        )
                    ),
                array(
                    'name' => 'Brendan Fairclough',
                    'last_name' => 'fairclough',
                    'born' => 'January 10, 1988 (age 27)',
                    'city' => 'Guildford, United Kingdom',
                    'bicycle' => 'Scott Gambler',
                    'place' => '8th (85.50)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/brendan.fairclough/',
                            'name' => '/brendan.fairclough'
                            ),
                        array (
                            'icon' => 'twitter',
                            'link' => 'https://twitter.com/brendog_1',
                            'name' => '@brendog_1'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/brendog1/',
                            'name' => 'brendog1'
                            )
                        ),
                    'photo' => 'p5pb12791689.jpg',
                    'photos' => array (
                        'p5pb12797172.jpg',
                        'p5pb12797247.jpg',
                        'p5pb12797256.jpg',
                        'p5pb12797352.jpg'
                        )
                    ),
                array(
                    'name' => 'Sam Reynolds',
                    'last_name' => 'reynolds',
                    'born' => 'June 4, 1991 (age 24)',
                    'city' => 'England, United Kingdom',
                    'bicycle' => 'Polygon DH9',
                    'place' => '9th (83.00)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/samreynolds26',
                            'name' => '/samreynolds26'
                            ),
                        array (
                            'icon' => 'twitter',
                            'link' => 'https://twitter.com/samreynolds26',
                            'name' => '@samreynolds26'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/samreynolds26/',
                            'name' => 'samreynolds26'
                            )
                        ),
                    'photo' => 'p5pb12791702.jpg',
                    'photos' => array (
                        'p5pb12797039.jpg',
                        'p5pb12797055.jpg',
                        'p5pb12797242.jpg',
                        'p5pb12797257.jpg',
                        'p5pb12797265.jpg',
                        'p5pb12797365.jpg',
                        'p5pb12797405.jpg'
                        )
                    ),
                array(
                    'name' => 'Remy Metailler',
                    'last_name' => 'metailler',
                    'born' => 'November 30, 1990 (age 24)',
                    'city' => 'Nice, France',
                    'bicycle' => 'Commencal Supreme DH',
                    'place' => '10th (82.50)',
                    'social' => array(
                        array (
                            'icon' => 'facebook',
                            'link' => 'https://www.facebook.com/remymetailler',
                            'name' => '/remymetailler'
                            ),
                        array (
                            'icon' => 'twitter',
                            'link' => 'https://twitter.com/remymetailler',
                            'name' => '@remymetailler'
                            ),
                        array (
                            'icon' => 'instagram',
                            'link' => 'https://instagram.com/remymetailler/',
                            'name' => 'remymetailler'
                            ),
                        array (
                            'icon' => 'youtube',
                            'link' => 'https://www.youtube.com/channel/UC_wCbFZHCh9amfaXXI4yq_g',
                            'name' => 'Rémy Métailler'
                            )
                        ),
                    'photo' => 'p5pb12791696.jpg',
                    'photos' => array (
                        'p5pb12797032.jpg',
                        'p5pb12797053.jpg',
                        'p5pb12797166.jpg',
                        'p5pb12797173.jpg',
                        'p5pb12797264.jpg',
                        'p5pb12797346.jpg'
                        )
                    )
                );
            echo json_encode(array('content' => $response, 'type' => 'riders_array'));
            break;
        
        default:
            $response = '<img src="images/red-bull-rampage-2015.png" alt="Red Bull Rampage 2015">
                <h1 class="date"><i class="fa fa-clock-o"></i> 15-16 October 2015</h1>
                <h1 class="location"><i class="fa fa-map-marker"></i> Virgin, Utah, United States</h1>';
            echo json_encode(array('content' => $response, 'type' => 'html'));
            break;
    }
}
else {

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Red Bull Rampage 2015</title>
    <link href="stylesheets/screen.css" media="screen, projection" rel="stylesheet" type="text/css">
    <script type="text/javascript" src="javascripts/jquery.min.js"></script>
    <script type="text/javascript" src="javascripts/script.js"></script>
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.png">
</head>
<body>
    <header>
        <nav class="top-menu">
            <div class="logo hidden">
                <a href="#home"><img src="images/red-bull-rampage-2015.png" alt="Red Bull Rampage 2015"></a>
            </div>
            <ul>
                <li><a href="#home">Home</a></li>
                <li><a href="#riders">Riders</a></li>
                <li><a href="#about">About</a></li>
                <li><a href="#rated">Rated Photos</a></li>
            </ul>
        </nav>
    </header>
    <nav class="right-menu hidden">
        <ul>
            <li><a href="#riders-sorge">Kurt Sorge</a></li>
            <li><a href="#riders-lacondeguy">Andreu Lacondeguy</a></li>
            <li><a href="#riders-agassiz">Graham Agassiz</a></li>
            <li><a href="#riders-semenuk">Brandon Semenuk</a></li>
            <li><a href="#riders-genon">Thomas Genon</a></li>
            <li><a href="#riders-zink">Cam Zink</a></li>
            <li><a href="#riders-berrecloth">Darren Berrecloth</a></li>
            <li><a href="#riders-fairclough">Brendan Fairclough</a></li>
            <li><a href="#riders-reynolds">Sam Reynolds</a></li>
            <li><a href="#riders-metailler">Remy Metailler</a></li>
        </ul>
    </nav>
    <div class="bg-main">
        <div class="center-wrapper">
            <div class="content-wrapper">
                <img src="images/red-bull-rampage-2015.png" alt="Red Bull Rampage 2015">
                <h1 class="date"><i class="fa fa-clock-o"></i> 15-16 October 2015</h1>
                <h1 class="location"><i class="fa fa-map-marker"></i> Virgin, Utah, United States</h1>
            </div>
        </div>
        <footer>
            Photographs come from the website <a href="http://www.pinkbike.com/">pinkbike.com</a>. Everything else created and developed by Adam Gołąb. &copy; 2015
        </footer>
    </div>
</body>
</html>
<?php

}

?>
